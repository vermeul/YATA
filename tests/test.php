<?php



$where = [
    "bla" => 'blu'
];

$where[] = 'blubber1';
array_push($where, 'blubber2');

var_dump($where);
die;


$data = <<<'EOT'
The start. Blalablaljka fjlsd
just before A{{#annot : as}}start A
    some text in a
    just before thirteen{{#annot: with id || 13}}thirteen starts right here.
    before X{{#annot: xs || }}
        text for x
    end of x{{#annotend:}}
    some more text for a
    before B{{#annot: bs||}}start b
        some text in b
        before C{{#annot: cs||}}start c
            some text in c
        end of c{{#annotend:}}
        some more text in b
        thirteen ends right there.{{#annotend:13}}
    end of b{{#annotend:}} 
    ending text of a{{#annotend:}}
this is THE END

EOT;

$where = array();

if ($where) {
    die("bla");
}
else {
    die("blu");
}
$blas = array("one", "two", "threee");
$where = array_merge($where, $blas);
#$where[] =  $blas;


var_dump($where);
die;



# match either {{#annot: or {{#annotend:
# fetch all options too
# ignore whitespace, except at the beginning
$annots_found = preg_match_all('/(?P<annotation>{{#annot(?P<end>end)?\s*:\s*(?P<opts>.*?)\s*}})/s', $data, $reg_params, PREG_OFFSET_CAPTURE);

function create_id() {
    $id = substr(
            str_shuffle(
                str_repeat(
                    '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ',
                    mt_rand(1,10)
                )
            
            ),
    1,4);
    #$id = substr(md5(rand()), 0, 4);
    #$id = substr(str_shuffle(MD5(microtime())), 0, 6);
    #$id = str_pad(dechex(mt_rand(0, 0xFFFFFF)), 6, '0', STR_PAD_LEFT);
    return $id;
}

$annotations_with_id = array();
$new_annotations = array();
$new_data = "";

# - run through all found {{#annot: and {{#annotend: matches
# - find every ending for every start, especially when no ID is present
# - automatically create a new unique ID if:
#   * no ID is present
#   * ID was not found in database
#

$starts = array();
$start_loc = 0;
$end_loc   = 0;
#if ($annots_found) {
    foreach($reg_params["annotation"] as $index => $value) {
        # $value[1] contains the location where the current tag has been found
        $end_loc = $value[1];
        $new_data = $new_data . substr($data, $start_loc, $end_loc-$start_loc);
        #print("end = $end_loc, start = $start_loc\n");
        #print substr($data, $start_loc, $end_loc-$start_loc);
        #print "\n------------------------------------\n";
        $start_loc = $value[1] + strlen($value[0]);
        # we encountered an annotation start
        if ($reg_params["end"][$index][0] === "") {
            list($comment, $category, $id) = preg_split('/\s*\|\s*/', $reg_params["opts"][$index][0]); 
            # we encountered an id
            if ($id) {
                $new_id = create_id();
                $annotations_with_id[$id] = $new_id;
                $new_annotations[$new_id] = array($value);
                $new_data = $new_data . "{{annot:$comment|$category|$new_id}}";
            }
            # we found no id, push it on the stack to find its ending counterpart later
            else {
                $new_id = create_id();
                $value["new_id"] = $new_id;
                array_push($starts, $value);
                $new_data = $new_data . "{{#annot:$comment|$category|$new_id}}";
            }
        }
        elseif ($reg_params["end"][$index][0] === "end") {
            $id = $reg_params["opts"][$index][0];
            # we encountered an id: try to find its beginning
            if ($id) {
                if ( $annotations_with_id[$id] ) {
                    $new_id = $annotations_with_id[$id];
                    array_push($new_annotations[ $new_id ], $reg_params["end"][$index]);
                    $text_start = $new_annotations[ $new_id ][0][1] 
                                + strlen($new_annotations[ $new_id ][0][0]);
                    $text_end   = $reg_params["annotation"][$index][1];
                    array_push(
                        $new_annotations[ $new_id ],
                        substr($data, $text_start, $text_end-$text_start)
                    );
                    $new_data = $new_data . "{{#annotend:$new_id}}";
                }
                else {
                    die("found an annotend with an id which has no corresponding start");
                }
            }
            # we found an ending, but have not enough starts: there seems to be a start-ending mismatch
            elseif ( count($starts) == 0 ) {
                die("starts and ends do not match!");
            }
            # we pop the last start from our stack to match it with its ending
            else {
                $start = array_pop($starts);
                $text_start = $start[1] + strlen($start[0]);
                $text_end   = $reg_params["annotation"][$index][1];
    
                $new_id = $start["new_id"];
                $new_annotations[$new_id] = array(
                    $start, 
                    $reg_params["end"][$index],
                    substr($data, $text_start, $text_end-$text_start)
                );
                $reg_params["annotation"][$index][2] = $new_id;
                $reg_params["annotation"][$index]["new_annot"] = "{{#annotend:$new_id}}";
                $new_data = $new_data . "{{#annotend:$new_id}}";
            }
        }
    }
    $new_data = $new_data . substr($data, $start_loc, strlen($data)-$start_loc);
#}
#else {
#    $new_data = $data;
#}
print $new_data;

#var_dump($reg_params["annotation"]);
#var_dump($new_annotations);


#foreach($reg_params["annotation"] as $value) {
#    $end = $value[1];
#    $new_data = $new_data . substr($data, $start, $end);
#    $new_data = $new_data . $value["new_annot"];
#    $start = $end + strlen($value[0]) + 1;
#}



exit;
die();

#preg_match_all('/(?<all>{{#annot:(?<comment>.*?)|(?<group>.*)|(?<id>)}})/', $data, $matches, PREG_OFFSET_CAPTURE);

# look where annotation starts. group and id might be optional. If id is optional, we will match it to the closest #annotend with no id either.
# We will add an id automatically.
#$matches_found = preg_match_all('/(?P<annot>{{#annot:\s*(?P<comment>.*?)\s*\|\s*(?P<group>.*?)\s*\|\s*(?P<id>.*?)\s*}})/', $data, $matches, PREG_OFFSET_CAPTURE);

# replace all {{#annotend}} or {{annotend}} with {{#annotend:}}, if necessary
$data = preg_replace( '/{{#*annotend}}/U','{{#annotend:}}', $data);


# look where annotation ends
$end_annot_found = preg_match_all('/(?P<annotend>{{#annotend:\s*(?P<id>.*?)\s*}})/s', $data, $end_annot, PREG_OFFSET_CAPTURE);
$annot_found = preg_match_all('/(?P<annot>{{#annot:.*?}})/s', $data, $annot, PREG_OFFSET_CAPTURE);

$annotations = [];
$annotation_ends = [];
$ends_with_id = [];
$ends_without_id = [];


if ($end_annot_found) {
    foreach ($end_annot["annotend"] as $match) {
        $id_found = preg_match('/{{#annotend:\s*(?P<id>.*)}}/s', $match[0], $ids);
        if ($ids["id"]) {
            $ends_with_id[$ids["id"]] = $match[1];
        }
        else {
            $ends_without_id[$match[1]] = true;
        }
    }
}

print("\n----------------- WIKITEXT--------------------------------\n");
print($data);
print("\n----------------------------------------------------------\n");

if ($annot_found) {
    foreach ($annot["annot"] as $match) {
        $params_found = preg_match_all('/{{#annot:\s*(?P<params>.*)}}/s', $match[0], $params);
        if ($params_found) {
            list($comment, $category, $id) = explode('|', $params["params"][0]);
            $categories = preg_split("/[,;:]/", $category);
            foreach ($categories as &$cat){
                $cat = trim($cat);
            }
            #print(var_dump($categories));
            $comment  = trim($comment);
            $category = trim($category);
            $id       = trim($id);
            $start    = $match[1] + strlen($match[0]);

            $annotations[$match[1]] = [
                "comment" => $comment,
                "category"=> $categories,
                "id"      => $id,
                "start"   => $start,
            ];

            if ($id ) {
                # we have an annotation id - we need to connect it to
                # its corresponding end
                if ($end = $ends_with_id[$id]) {
                    $annotations[$match[1]]["end"] = $end;
                    $annotations[$match[1]]["wikitext"] = 
                        substr($data, $start, $end - $start);
                }
                else {
                    # we didn't find any corresponding end: throw an error
                    die("no end found for id: $id");
                }
            }
            else {
                # look for annotation ends without an id 
                # which are nearest of the start of our annotation
                foreach($ends_without_id as $annot_end => $value){
                    if($annot_end > $start) {
                        $annotations[$match[1]]["end"] = $annot_end;
                        $annotations[$match[1]]["wikitext"] = 
                            substr($data, $start, $annot_end - $start);
                        break;
                    }
                }
            }
        }
    }
}

print_r($annotations);

