# YATA - Yet Another Text Annotation

YATA is a text annotation plugin for MediaWiki. It allows to annotate text passages with comments.
Each annotation can belong to a category. And categories can be organized in a hierarchical way.
In addition, annotations might be either nested or overlapping. Because annotations become part of
the text, the text itself can be edited anytime, unlike most alternatives (see below).

If you want to annotate text which _cannot be edited after_, there are several alternatives:

- GATE (https://gate.ac.uk) – open source software capable of solving almost any text processing problem
- co-ment (http://www.co-ment.com) – co-ment is a web service for annotating, discussing and writing texts online
- CATMA (http://catma.de) – for undogmatic textual markup and analysis

## Installation

- put the extension in the `extension` folder of your mediawiki installation
- add `wfLoadExtension( 'YATA' );` to your `LocalSettings.php`
- run `php maintenance/update.php` to add the necessary tables to your database

## Text Annotation

Text Annotations must be distinguished from
[Semantic Annotations](https://www.semantic-mediawiki.org/wiki/Help:Introduction_to_Semantic_MediaWiki).
Semantic Annotations put an singular (atomic) information into a context, for example, a number which an either represent a height, population, age etc.
Semantic Media Wiki enables you to add this kind of information to a piece of your text and offers you the possibility to list all pages
of cities in Europe with a population greater than one Million, for example. It also allows you to directly extract the relevant information
(Name of the City, Country, Population, coordinates) from the Wiki Pages that are found.

YATA does something similar. It annotates certain sections of a text and puts them in a context. On the other hand,
Text Annotations can overlap and/or be nested, something which is not possible with Semantic Annotations.
You can add an arbitary comment as well as categorize your annotation (multiple categorizations are possible).
Categories can be organised in a hierarchical structure. When you query your annotations, your result does
not only are take to the right page but also to the exact position of the annotation.

### Annotation Syntax

Annotations always have a start and an end. They look like this:

```
{{#annot:}}            # opening tag, which marks the start of an annotation
{{#annotend:}}         # closing tag, which marks the end of an annotation
```

Annotations usually contain comments and may be categorised (Categories: see below):

```
{{#annot: comment}}                         # basic annotation with just a comment
{{#annot: comment | category}}              # categorized annotation (see annotaion categories below)
{{#annot: comment | cat_parent/cat_child}}  # annotation with a specific category identified by a `cat_parent/cat_child` pair
{{#annot: comment | category1, category2}}  # annotation with more than one category
{{#annot: comment | category | some_id}}    # an id is used to identify the proper end of the annotation when nesting. Annotation end must contain the same id: `{{#annotend: some_id}}`
{{#annot: | category}}                      # just specifiying the category a text belongs to, with no comment
{{#annot: | #category_hashtag }}            # the hashtag is a quicker way to access annotation categories that have large names
{{#annot: comment | | some_id}}             # nested annotation with an id to identify its ending, without any category

{{#annotend: id}}                           # End-of-Annotation-Tag. The id must be present if the opening tag has one.
{{annotend}}      {{#annotend}}             # some syntax sugar in case you forget the colon or the hash symbol
```

### Annotation per page

```
{{#annotpage: list }}                       # will list all annotations on that page
{{#annotpage: key=value, key2="value 2"}}   # store page attributes
```

### Nested or overlapping annotations: Examples

The vertical bars are used to supply a number of different arguments. Because annotations can be even nested or overlapping,
we need to add an identifier to be sure the start and endings are connected together correctly. For example:

```
Here is some {{#annot: comment1 | category1 | a}} more text
which has even {{#annot: comment2 | category2 | b}} overlapping
comments{{#annotend: a}}, which means we need {{#annotend: b}} to
connect start and ending of every comment.
```

Annotation (a) then contains:

```
more text
which has even overlapping
comments
```

while annotation (b) will contain this part of the WikiText:

```
 overlapping
 comments, which means we need
```

### display all annotations of a given page

To display all annotations on a page, just add the magic word

```
{{#annotlist:}}
{{#annot: list}}  # alternative
```

somewhere in your Wikicode, preferably at the beginning or end of your page.
The output will be a sortable Wikitable which contains comment, category and
the extracted text.

If you want to display all **page annotations**, add the magic word

```
{{#annotpage: list}}
```

to your Wikipage.

## Querying annotations

To get specific annotations we are interested in, we need a small query language
which allow us to find a text passage in a page anywhere in the Wiki. Our goal is
to define the query language similar to the Semantic Media Wiki query language {{#ask:}}.
See the [SMW concepts](https://www.semantic-mediawiki.org/wiki/Help:Concepts)

### annotask Query syntax

```
[[category: category_name1 ]]                  # this category (and all categories below it)
[[category: category_name1, category_name2 ]]  # category_name1 OR category_name2
[[category: #category_tag1, #category_tag2 ]]  # category hashtags can be used as well

[[category: #parent_category[0] ]]             # only match annotations that are marked belonging to #parent_category,
                                               # not to any existing child-categories. If the number is 1, only its
                                               # direct children categories should be included.

[[category: category_name1 ]]                  # every line is AND'ed, so these two lines mean:
[[category: category_name2 ]]                  # category_name1 AND category_name2

[[category!: category_name1 ]]                 # category is NOT category_name1
[[wiki_text: %anything% ]]                     # wikitext LIKE %anything%
[[wiki_text: %anyth_ng% ]]                     # Underscore matches single characters
[[wiki_text!: %anything% ]]                    # wikitext NOT LIKE %anything%

[[page: place=Constantinople ]]                # pages that have place=Constantinople
[[page: time_from  >=1450]]                    # and time higher than 1450
[[page: time_until <=1526]]                    # and time lower than 1526

[[page: place="Constantinople", "Zurich"]]     # place is either Constantinople or Zurich

[[verbose: 1]]                                 # show a verbose output of the query.
[[show_pa: author, time]]                      # a list of page attributes that should appear as columns (hide all others)
[[hide_pa: city, country]]                     # a list of page attributes that should be hidden (show all others)

! "not" ("unequal")
% match any number of characters
_ match any single character
```

### Annotation query example

```
{{#annotask:
	[[category: parent_category1/child_category1 ]]
	[[category!: #some_unwanted_category ]]
	[[comment: %a part of my comment% ]]
	[[wiki_text: %this is somewhere in my text%]]
}}
```

Find all annotations

- which belong to category `category parent_category1/child_category1`
- but not to category `#some_unwanted_category`
- and contain the comment `a part of my comment`
- and contain `this is somewhere in my text` in the extracted annotation

### annotask: Query result

The result is a sortable table containing the following data:

`Category | Comment | Link | Annotated Text | Last edited by | Modification date`

Because Pages can be long, the link contains an anchor (bookmark) and thus the result will appear immediately at the top of the page.
To achieve this, all annotations automatically get a bookmark generated which will result in a unique `id` within that page.

## Annotation Categories

Annotations can be categorized, but don't have to be. The categories themselves are organized hierarchically.
If a category has no parent category, it is a top category. If a category has no child category, it is called a leaf.
If a category has both a parent and a child category, it is called a node.
Categories themselves don't have to be unique. In the example below, the category `examples` is not unique, one
belongs to its parent `written addition` and the other to `written subtraction`:

```
/addition
    /written addition
        /examples
/subtraction
   /written subtraction
        /examples
```

So if we want to annotate a written subtraction example, we simply write:

```
{{#annot: my comment | written subtraction/examples}}
```

Note: **the combination of parent_category/child_category must be unique**

### list categories

Put this on any WikiPage and it will list existing categories:

```
{{#annotcat: list}}                              # lists all categories (might be a long list!)
{{#annotcat: list | top_category_name}}          # list a certain top category and all categories «below»
{{#annotcat: list | parent_name/category_name}}  # start list with node parent_category/category
```

### add a new category

To add a new category into the categories table, we simply add one of the following anywhere in a Wiki Page:

```
{{#annotcat: add | name='new top category', hashtag='#my_top_cat', description='some meaningful comments', parent='grandparent_category/parent_category' }}
{{#annotcat: add | name='new top category', hashtag='#my_top_cat', description='some meaningful comments', parent='parent_category' }}  # add to an existing top category
```

Only the name argument is mandatory, everything else is optional. If you don't specify a parent category, a new top category will be created.

### update an existing category

To update a category, add something like this to any Wiki Page:

```
# update everything
{{#annotcat: up | kat_parent/kat_child | name='new category name', description='new description', parent='grandparent_category/parent_category' }}
{{#annotcat: up | #category_hashtag | name='new category name', description='new description', parent='#parent_category_hashtag ' }}
# make a category becoming a top category
{{#annotcat: up | kat_parent/kat_child | parent=}}
{{#annotcat: up | #category_hashtag | parent=}}
# remove description
{{#annotcat: up | kat_parent/kat_child | description=}}
{{#annotcat: up | #category_hashtag | description=}}
```

If you do not want to update certain fields, just omit them. If you want to remove the parent or deleting the description, just leave the value empty.

### delete an existing category

To delete a category, add this to any Wiki Page:

```
{{#annotcat: del | parent_cat_name/cat_name }}
{{#annotcat: del | #category_hashtag }}
{{#annotcat: del | cat_name }}  # if the category is a top category (no parent)

```

It will delete the category from the database. Annotations will still be there, but no longer assigned to a category.
If this category was a parent- or child-category, then this connection will break and you might not receive the same
results as before when searching for annotations. Therefore: be careful!
