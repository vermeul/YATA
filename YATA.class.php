<?php
/**
 * YATA extension
 * @author Swen Vermeul
 * @file
 * @version 0.1
 * @license GNU General Public Licence 2.0
 */
use Math\Renderer;

class YATA {
    public static $annotations = "";
    public static $page_annotations = "";
    //
    // Register any render callbacks with the parser
    //
    public static function onParserSetup( &$parser ) {

       // Create a function hook associating the "example" magic word with renderExample()
       $parser->setFunctionHook('annot',     'YATA::renderStartAnnot');
       $parser->setFunctionHook('annotend',  'YATA::renderEndAnnot');
       $parser->setFunctionHook('annotlist', 'YATA::annotations_list');
       $parser->setFunctionHook('annotask',  'YATA::annotation_query');
       $parser->setFunctionHook('annotcat',  'YATA::annotation_categories');
       $parser->setFunctionHook('annotpage', 'YATA::annotation_page');
    }

    //
    // Render the output of {{#annot: comment | category | id}}.
    //
    public static function renderStartAnnot( $parser, $comment='', $category='', $id='' ) {
        if ($comment === 'list') {
            return self::annotations_list($parser);    
        }
        else {
            return "<span id=$id title='$comment'>";
        }
    }

    //
    // Render the output of {{#annotend:}}.
    //
    public static function renderEndAnnot( $parser, $id='' ) {
       return "</span>";
    }
    
    //
    // Render the output of {{#annotask:}}
    //
    public static function annotation_query( $parser, $querystring) {


        $title = $parser->getTitle();
        $wikiPage = new WikiPage( $title );
        $queries_found = preg_match_all(
            '/\[\[(?P<query>.*?)\]\]/s',
            $querystring, 
            $query_params, 
            PREG_OFFSET_CAPTURE
        );
        $searches = array();
        $in_categories = array();
        $search_in_annotations = false;

        $where = array();
        $dbr = wfGetDB( DB_REPLICA );
        $seen = array();
        $errors = array();
        $hide_pa_columns = array();
        $show_pa_columns = array();
        $verbose = 0;
        foreach($query_params["query"] as $query) {
            list($field, $values) = preg_split('/\s*\:\s*/', $query[0]);
            # syntax-sugar
            if ($field === 'wikitext') {
                $field = 'wiki_text';
            }
            elseif ($field === 'verbose') {
                $verbose = 1;
            }
            elseif ( $field === 'category') {
                # multiple categories are defined by using a comma ,
                # between two categories.
                $categories = preg_split('/\s*\,\s*/', $values);
                $cat_ids = array();
                foreach( $categories as $category_name ) {
                    # keep category name for display later
                    #array_push($in_categories, $category_name);

                    # remove the search-depth from the category name, e.g.
                    # category_name[1] --> category_name   1
                    preg_match(
                        '/(?P<category>.*)(\[(?P<depth>\d+)\])|(?P<category_all>.*)/',
                        $category_name, $matches, PREG_OFFSET_CAPTURE
                    );

                    $category = array_key_exists('category_all', $matches) ? $matches['category_all'][0] : $matches['category'][0];
                    $depth    = array_key_exists('depth', $matches)        ? $matches['depth'][0]        : null;


                    $cats = self::get_categories($dbr, $category);
                    if (! is_array($cats) ) {
                        array_push($errors, "no such category found: $category");
                        continue;
                    }
                    if (! $cats[0]) {
                        array_push($errors, "no such category found: $category");
                        continue;
                    }
                    foreach($cats as $cat) {
                        $in_categories[] = $cat->name;
                        $cat_ids[] = $cat->id;
                        if ($depth) {
                            $child_categories = self::get_child_categories($dbr, $cat, 0, $depth-1);
                            foreach($child_categories as $child_category) {
                                $cat_ids[] = $child_category->id;
                                $in_categories[] = $child_category->name;
                            }
                        }
                        elseif ($depth === "") {
                            $child_categories = self::get_child_categories($dbr, $cat, 0, null);
                            foreach($child_categories as $child_category) {
                                $cat_ids[] = $child_category->id;
                                $in_categories[] = $child_category->name;
                            }
                        }
                    }

                }
                $where[] = 
                    "EXISTS( 
                        SELECT NULL 
                        FROM yata_annotation_category ac 
                        WHERE ac.annotation_id = a.id
                        AND ac.category_id IN ("
                    . join(',', $cat_ids)
                    . ")
                    )";
                $search_in_annotations = true;
            }
            elseif( $field === 'user' ) {
                $where["u.username"] = $values;
            }
            elseif( $field == 'hide_pa' ) {
                $hide_pa_columns = preg_split('/\s*\,\s*/', $values);
            }
            elseif( $field == 'show_pa' ) {
                $show_pa_columns = preg_split('/\s*\,\s*/', $values);
            }
            # page annotation
            elseif( $field === 'page' ) {
                $key_comp_vals = self::extract_query_keyvalues($values);
                foreach($key_comp_vals as $key_comp_val){
                    $where[] =
                        "EXISTS(
                            SELECT NULL
                            FROM yata_page_annotation pa
                            WHERE pa.page_id = p.page_id"
                            ." AND pa.name = "
                            .$dbr->addQuotes($key_comp_val[0])
                            ." AND pa.value "
                            .$key_comp_val[1]
                            .$dbr->addQuotes($key_comp_val[2])
                        .")";
                }
            }
            # annotation
            elseif( in_array($field, array('wiki_text', 'comment')) ) {
                array_push($searches, $field . ': ' . $values);
                if ( preg_match( '/[\%_]/', $values ) ) {
                    $where[] = "a.$field LIKE (" . $dbr->addQuotes($values).")";
                }
                else {
                    $where["a.$field"] = $values;
                }
                $search_in_annotations = true;
            }
        }

	    if ($errors) {
            foreach( $errors as $error) {
                $errstr .= $error . "<br/>";
            }
            return $errstr;
        }

        $annotations = null;
	    if ($search_in_annotations) {
            # fetch the annotation details
            $annotations = $dbr->select(
                array(
                    'p' => 'page',
                    'a' => 'yata_annotation',
                    'u' => 'user',
                    'ac'=> 'yata_annotation_category',
                    'c' => 'yata_category',
                    'pc'=> 'yata_category',
                ),
                array(
                    'page_id'         => "p.page_id",
                    'category'        => "c.name",
                    'parent_category' => "pc.name",
                    'hashtag'         => "c.hashtag",
                    'title'           => "p.page_title",
                    'comment'         => "a.comment",
                    'wiki_text'       => "a.wiki_text",
                    'start_char'      => "a.start_char",
                    'bookmark'        => "a.bookmark",
                    'last_edited_by'  => "u.user_name",
                    'last_modified'   => "a.modified_date",
                ),
                $where,
                __METHOD__,
                array(
                    'ORDER BY' => 'c.name ASC'
                ),
                array(
                    'u'  => array( 'LEFT JOIN', array( 'u.user_id = a.user_id') ),
                    'ac' => array( 'LEFT JOIN', array( 'ac.annotation_id = a.id' ) ),
                    'c'  => array( 'LEFT JOIN', array( 'ac.category_id = c.id' ) ),
                    'pc' => array( 'LEFT JOIN', array( 'pc.id = c.parent_id') ),
                    'p'  => array( 'LEFT JOIN', array( 'p.page_id = a.page_id') ),
                )
            );
        }
	    else {
            # fetch the annotation details
            $annotations = $dbr->select(
                array(
                    'p' => 'page',
                ),
                array(
                    'page_id'         => "p.page_id",
                    'category'        => "NULL",
                    'parent_category' => "NULL",
                    'hashtag'         => "NULL",
                    'title'           => "p.page_title",
                    'comment'         => "NULL",
                    'wiki_text'       => "NULL",
                    'start_char'      => "NULL",
                    'bookmark'        => "NULL",
                    'last_edited_by'  => "NULL",
                    'last_modified'   => "NULL",
                ),
                $where,
                __METHOD__,
                array(
                ),
                array(
                )
            );
        }
	    //print($dbr->lastQuery($annotations));

        $table = "";
        if ($verbose && $searches) {
            $table .= "<b>Searched for term:</b>";
            foreach($searches as $search_string) {
                $table .= "<pre>".$search_string."</pre>";
            }
        }
        if ($verbose && $in_categories) {
            $table .= "<b>Searched in these categories:</b>";
            $table .="<pre>";
            foreach($in_categories as $category) {
                $table .= "\n".$category;
            }
            $table .="</pre>";
        }


        $max_no_cats = 0;
	    if ($dbr->numRows($annotations) > 0) {
            $page_attributes = array();
            $column_no = 0;
            $seen_annot = array();
            $categories = array();

            # compose the wiki table
            foreach($annotations as $annotation) {
                # if a category has no parent, just show the category name
                # in all other cases show parent_category:child_category
                $cat = $annotation->parent_category ? $annotation->parent_category . "/" . $annotation->category : $annotation->category;

                $wikitext = $annotation->wiki_text;
                $wikitext = str_replace('|style="text-align:right;"|', '', $wikitext);
                $wikitext = htmlentities($wikitext, ENT_QUOTES);

                $a = array('=', '{', '|', '}');
                $b = array('&#61;', '&#123;', '&#124;', '&#125;');
                $wikitext = str_replace($a, $b, $wikitext);
                $link = null;
                if ($annotation && $annotation->bookmark) {
                    $link = "[[".$annotation->title."#".$annotation->bookmark."]]";
                }
                else {
                    $link = "[[".$annotation->title."]]";
                }

                $wikitext = html_entity_decode($wikitext);
                $rendered_wikitext = preg_replace_callback(
                    '/(?P<math_element><math>(?:.*?)<\/math>)/s',
                    'self::render_math_content',
                    $wikitext
                );
                $rendered_wikitext = 'ENCODED_YATA_CONTENT ' 
                                   . base64_encode($rendered_wikitext)
                                   . ' END_ENCODED_YATA_CONTENT';

                $table_row = array(
                    "categories" => array($cat),
                    "comment"  => $annotation->comment,
                    "link"     => $link,
                    "annot"    => $rendered_wikitext,
                );

                $p_annotations = self::get_page_annotations($dbr, $annotation->page_id);
                foreach ($p_annotations as $p_annotation) {
                    if (! in_array($p_annotation->name, $page_attributes)) {
                        # got a hide_pa: show all columns except the ones indicated
                        # hide has precedence over show
                        if ($hide_pa_columns) {
                            if (! in_array($p_annotation->name, $hide_pa_columns)) {
                                array_push($page_attributes, $p_annotation->name);
                            }
                        }
                        # got a show_pa: show only columns indicated
                        elseif ($show_pa_columns) {
                            if (in_array($p_annotation->name, $show_pa_columns)) {
                                array_push($page_attributes, $p_annotation->name);
                            }
                        }
                    }
                    $table_row[$p_annotation->name] = $p_annotation->value;
                }
                # merge annotations hat appear under several categories
                if (isset($seen_annot[$link])) {
                    array_push($seen_annot[$link]["categories"], $cat);
                }
                else {
                    $seen_annot[$link] = $table_row;
                }
                $max_no_cats = sizeof($seen_annot[$link]["categories"]) > $max_no_cats ? sizeof($seen_annot[$link]["categories"]) : $max_no_cats;
            }

            if ($verbose) {
                $table .= "<b>" . count($seen_annot) ." Results of annotask query: </b>";
                $table .= '([' . $title->getFullURL() . '?action=purge' . ' refresh page]):';
            }

            $new_table = '<table class="wikitable sortable jquery-tablesorter">';
            $table_header = '
    <tr>';
            if ($max_no_cats > 0) {
                for ($x = 1; $x <= $max_no_cats; $x++) {
                    $table_header .= '<th class="headerSort headerSortUp" tabindex="0" role="columnheader button" title="Sort descending"> Category </th>';
                }
            }
            $table_header .= '
        <th class="headerSort" tabindex="0" role="columnheader button" title="Sort ascending"> Comment </th>
        <th class="headerSort" tabindex="0" role="columnheader button" title="Sort ascending"> Link </th>
        <th class="headerSort" tabindex="0" role="columnheader button" title="Sort ascending"> Annotated text </th>';

            $build_th = function($val){
                return "<th>" . $val . "</th>";
            };
            $build_td = function($val) {
                return "<td>" . $val . "</td>";                 
            };
            $all_columns = array();

            $all_columns = array_merge(['comment','link','annot'],$page_attributes);

            $table_header .= join("", array_map($build_th, $page_attributes));
            $table_header .= "</tr>";
            $table .= $new_table . $table_header;

            $rows_rendered = "";
            foreach($seen_annot as $link => $table_row) {
                $rows_rendered .= "<tr>";
                if ($max_no_cats > 0) {
                    for ($x = 0; $x < $max_no_cats; $x++) {
                        $rows_rendered .= "<td>" . $table_row['categories'][$x] . "</td>";
                    }
                }
                foreach($all_columns as $column) {
                    $rows_rendered .= "<td>";
                    if (isset($table_row[$column])) {
                        $rows_rendered .= $table_row[$column];
                    }
                    $rows_rendered .= "</td>";
                }
                $rows_rendered .= "</tr>";
            }

            $table .= $rows_rendered;
            $table .= "</table>";
        }
        else {
            $table .= "<b>No results found.</b>";
        }

        return $table;
    }


    //
    // Render the output of {{#annotlist:}}.
    //
    public static function annotations_list( $parser ) {
        $title = $parser->getTitle();
        $wikiPage = new WikiPage( $title );
        #$dbr = wfGetDB( DB_REPLICA );
        $dbr = wfGetDB( DB_MASTER );

        $table = <<<"EOT"
{| class="wikitable sortable"
|-
! Category !! Hashtag !! Comment !! Link !! Annotated text !! Last edited by !! Modification date

EOT;
        # fetch the annotation details
        $annotations = $dbr->select(
            array(
                'a' =>'yata_annotation',
                'ac'=>'yata_annotation_category',
                'c' =>'yata_category',
                'pc'=>'yata_category',
                'u' =>'user',
            ),
            array(
                'comment'         => "a.comment", 
                'wiki_text'       => "a.wiki_text",
                'start_char'      => "a.start_char",
                'bookmark'        => "a.bookmark",
                'category'        => "c.name",
                'hashtag'         => "c.hashtag",
                'parent_category' => "pc.name",
                'last_edited_by'  => "u.user_name",
                'last_modified'   => "a.modified_date"
            ),
            array(
                'a.page_id' => $wikiPage->getId(),
            ),
            __METHOD__, 
            array(
                'ORDER BY' => 'a.start_char ASC' 
            ), 
            array( 
                'u'  => array( 'LEFT JOIN', array( 'u.user_id = a.user_id') ),
                'ac' => array( 'LEFT JOIN', array( 'ac.annotation_id = a.id' ) ),
                'c'  => array( 'LEFT JOIN', array( 'ac.category_id = c.id' ) ),
                'pc' => array( 'LEFT JOIN', array( 'pc.id = c.parent_id') ),
            )
        );

        # compose the wiki table
        foreach($annotations as $annotation) {
            # if a category has no parent, just show the category name
            # in all other cases show parent_category:child_category
            $wikitext = preg_replace(
                '/{{\s*#annot(?:<end>end)?.*?}}/', 
                '',
                $annotation->wiki_text
            );
            $cat = $annotation->parent_category ? $annotation->parent_category . "/" . $annotation->category : $annotation->category;
            $table .= "|-\n";
            $table .= "| ".$cat
                   ." || ".$annotation->hashtag
                   ." || ".$annotation->comment 
                   ." || [[$title#".$annotation->bookmark . "]]"
                   ." || " . $wikitext
                   ."\n || " . $annotation->last_edited_by
                   ."\n || " . $annotation->last_modified
                   ."\n";
        }
        $table .= "|}";
        return $table;
        
    }

    //
    // Render the output of {{#annotcat: list}}.
    //
    public static function annotation_categories( $parser, $method, $start_with=null ) {
        if ($method  != "list") {
            return "";
        }
        $dbr = wfGetDB( DB_MASTER );

        $where = array('parent_id' => null);  # start with all top catgegories
        if ( $start_with ) {
            $start_with_cat = self::get_category($dbr, $start_with);
            if ( $start_with_cat ) {
                $where = array('id' => $start_with_cat->id);
            }
        }
                        
        $categories = self::search_categories($dbr, $where, array('ORDER BY' => 'name') );


        $table = <<<"EOT"
{| class="wikitable sortable"
|-
! Category !! Hashtag !! Description !! id !! parent_id

EOT;

        foreach($categories as $category) {
            # if a category has no parent, just show the category name
            # in all other cases show parent_category:child_category
            $table .= "|-\n";
            $table .= "| ".$category->name
                   ." || ".$category->hashtag
                   ." || ".$category->description
                   ." || ".$category->id
                   ." || ".$category->parent_id
                   ."\n";
            $child_categories = self::get_child_categories($dbr, $category, 1);
            if ($child_categories) {
                foreach ($child_categories as $child_category) {
                    $table .= "|-\n";
                    $table .= "|" . str_repeat('&nbsp;&nbsp;&nbsp;', $child_category->level) . $child_category->name
                        ." || ".$child_category->hashtag
                        ." || ".$child_category->description
                        ." || ".$child_category->id
                        ." || ".$child_category->parent_id
                        ."\n";
                }
            }

        }
        $table .= "|}";
        return $table;

    }

    //
    // Rendering of {{#annotpage: list}}
    //
    public static function annotation_page( $parser, $method ) {
        if ($method != "list") {
            return "";
        }
        $title = $parser->getTitle();
        $wikiPage = new WikiPage( $title );
        $wikiPage->doPurge();
        $dbr = wfGetDB( DB_MASTER );

        $annotations = self::get_page_annotations($dbr, $wikiPage->getId() );

        $table = <<<"EOT"
{| class="wikitable sortable"
|-
! Name !! Value !! Creation Date !! Created By

EOT;

        foreach($annotations as $annotation) {
            # if a annotations has no parent, just show the annotations name
            # in all other cases show parent_annotations:child_annotations
            $table .= "|-\n";
            $table .= "| ".$annotation->name
                ." || ".$annotation->value
                ." || ".wfTimestamp( TS_ISO_8601, $annotation->insert_date)
                ." || ".$annotation->user_name
                ."\n";
        }
        $table .= "|}";
        return $table;

    }


    public static function onPageContentSave(WikiPage &$wikiPage, User
    &$user, Content &$content, $summary, $isMinor, $isWatch, $section, &$flags, Status &$status ){
    /*
        onPageContentSave: Hook on when page is saved
        we first replace syntax sugar where necessary, e.g.
        - change {{#annotend}} to {{#annotend:}}
        - change {{annotlist}} or {{#annotlist}} to {{#annotlist:}}

        next steps:
        1. add/delete/update categories (see manage_categories)
        2. parse the annotations (see parse_annotations)
        3. replace the wiki_text 
        
        The annotations are saved in the next hook: onPageContentSaveComplete (see below)

    */
        if ($content->getModel() != "wikitext") {
            return true;
        }

        $data = $content->getNativeData();

        # get database handler
        $dbw = wfGetDB( DB_MASTER );
        $dbr = wfGetDB( DB_REPLICA );
        if ( $content instanceof TextContent ) {

            $data = $content->getNativeData();
            # replace all {{#annotend}} or {{annotend}} with {{#annotend:}}, where necessary
            # replace all {{#annotlist}} or {{annotlist}} with {{#annotlist:}}, where necessary
            $data = preg_replace( '/{{#*(annotend|annotlist)}}/U','{{#$1:}}', $data);

            # 1. add / delete categories 
            # 2. remove these order from the wikiText,
            #    because they are used only once.
            try {
                $data = self::manage_categories($dbw, $data);
            }
            catch (Exception $e) {
               $status->fatal($e->getMessage());
               return false;
            }

            # 2. get all annotations that exist in the current wikitext
            try {
                list($annotations, $page_annotations) = self::parse_annotations($dbw, $data, $wikiPage);
            }
            catch (Exception $e) {
               $status->fatal($e->getMessage());
               return false;
            }

            # keep the annotations for saving later (onPageContentSaveComplete)
            self::$annotations = $annotations;
            self::$page_annotations = $page_annotations;

            # 3. replace the content and save it
            $content = new WikitextContent( $data );
        }

        if ( !$status->isOK() ) {
            return false;
        }
        return true;
    }

    /*
    This function is called after the save page request has been processed.
    @see https://www.mediawiki.org/wiki/Manual:Hooks/PageContentSaveComplete
    
    - save all new or update changed annotations
    - Annotations which are no longer present should be deleted.
    - all entries in annotation_category which are no longer used should be deleted.
    */
    public static function onPageContentSaveComplete($wikiPage, $user, $content, $summary,
    $isMinor, $isWatch, $section, $flags, $revision, $status, $baseRevId ) {
        $dbw = wfGetDB( DB_MASTER );
        $seen_bookmarks = array();
        $seen_annotation_ids = array();
       $i = 0; 
        foreach ( self::$annotations as $bookmark => $annotation ){
            $annotation_id = 0;
            $ex_annot = self::get_annotation($dbw, $wikiPage, $bookmark);

            # existing annotation found: update wiki_text, comment
            if ($ex_annot) {
                $annotation_id = $ex_annot->id;
                if ($ex_annot->wiki_text != $annotation["wiki_text"] 
                     or $ex_annot->comment != $annotation["comment"] ) {
                    $dbw->update(
                        'yata_annotation',
                        array(
                            'wiki_text' => $annotation["wiki_text"],
                            'comment'   => $annotation["comment"],
                            'user_id'   => $user->getId(),
                            'modified_date' => $dbw->timestamp(),
                        ),
                        array(
                            'page_id'  => $wikiPage->getId(),
                            'bookmark' => $bookmark
                        )
                    );
                }
            }
            # new annotation found: insert annotation
            else {
                # write annotation
                try {
                    $data = array(
                            'page_id'       => $wikiPage->getId(),
                            'comment'       => $annotation['comment'],
                            'wiki_text'     => $annotation['wiki_text'],
                            'bookmark'      => $bookmark,
                            'user_id'       => $user->getId(),
                            'insert_date'   => $dbw->timestamp(),
                            'modified_date' => $dbw->timestamp(),
                    );
            
                    $dbw->insert(
                        'yata_annotation',
                        array(
                            'page_id'       => $wikiPage->getId(),
                            'comment'       => $annotation['comment'],
                            'wiki_text'     => $annotation['wiki_text'],
                            'bookmark'      => $bookmark,
                            'user_id'       => $user->getId(),
                            'insert_date'   => $dbw->timestamp(),
                            'modified_date' => $dbw->timestamp(),
                        )
                    );
                    $ex_annot = self::get_annotation($dbw, $wikiPage, $bookmark);
                    $annotation_id = $ex_annot->id;
                }
                catch (Exception $e) {
                    print($e->getMessage());
                }
            }
            # keep track of all the annotations we have seen on this page,
            # to delete later all the annotations which have been removed from this page.
            array_push($seen_bookmarks, $bookmark);
            array_push($seen_annotation_ids, $annotation_id);


            # insert / update the annotation categories
            if ( $annotation['categories'] ) {
                $seen_acs = array();
                foreach ( $annotation['categories'] as $category ){
                    # assign all found categories to this annotation
                    if(! is_null($category)) {
                        self::insert_annotation_category($dbw, $wikiPage, $annotation_id, $category->id);
                        array_push($seen_acs, $category->id);
                    }
                }
                # delete all annotation categories which are no longer assigned
                # to this annotation
                $dbw->delete(
                    'yata_annotation_category',
                    array( 
                        'page_id'       => $wikiPage->getId(),
                        'annotation_id' => $annotation_id,
                        'category_id NOT IN(' .$dbw->makeList($seen_acs). ')'
                    )
                );
            }
            else {
                # delete all existing annotation categories,
                # since no category is assigned anymore
                $dbw->delete(
                    'yata_annotation_category',
                    array(
                        'page_id'       => $wikiPage->getId(),
                        'annotation_id' => $annotation_id,
                    )
                );
            }
        }
        

        # find all all annotations on that page...
        $where = [
            "page_id" => $wikiPage->getId()
        ];
        #...which are no longer present
        if ($seen_annotation_ids) {
            $where[] = 'id NOT IN(' .$dbw->makeList($seen_annotation_ids). ')';
        }

        $annotations_to_delete = $dbw->select(
            'yata_annotation',
            array( 'id' ),
            $where
        );


        # delete all annotations which are no longer present
        foreach( $annotations_to_delete as $annotation) {
            $dbw->delete(
                'yata_annotation',
                array(
                    'id' => $annotation->id 
                )
            );

            # delete all categories assigned to annotations
            # which do not exist anymore

            $delete_where = [
                'page_id'       => $wikiPage->getId(),
                'annotation_id' => $annotation->id
            ];

            $dbw->delete(
                'yata_annotation_category',
                $delete_where
            );
        }

        $ex_page_annotations = self::get_page_annotations($dbw, $wikiPage->getId());
        $ex_annotations = array();
        if ($ex_page_annotations) {
            foreach ($ex_page_annotations as $ex_page_annotation) {
                $ex_annotations[$ex_page_annotation->name] = $ex_page_annotation->value;
            }
        }

        foreach( self::$page_annotations as $key => $value) {
            if (array_key_exists($key, $ex_annotations)){
                if ($ex_annotations[$key] != $value) {
                    self::update_page_annotation($dbw, $wikiPage, $key, $value);
                }
                else {
                    # value has not changed - do nothing
                }
                unset($ex_annotations[$key]);
            }
            else {
                self::insert_page_annotation($dbw, $wikiPage, $user, $key, $value);
            }
        }

        # remove the keys and values that are no longer in use.
        foreach( $ex_annotations as $key => $value) {
            self::delete_page_annotation($dbw, $wikiPage, $key, $value);
        }

    }

    public static function add_category($dbw, $category_name, $hashtag, $description, $parent) {
        $category_name = trim($category_name);
        if (preg_match("/[\/\\,;]/", $category_name)) {
            throw new Exception("following characters are not allowed in a category name: / \ , ;");
        }
        $parent_id = null;
        if (! is_null($parent)) {
            $parent_category = self::get_category($dbw, $parent);
            if(! $parent_category) {
                throw new Exception("Error adding new category: no such parent-category found: $parent");
            }
            $parent_id = $parent_category->id;
        }
        $exists = FALSE;
        if ($hashtag) {
            $exists = self::get_category($dbw, $hashtag);
        }
        else {
            $exists = self::get_category($dbw, $category_name);
        }
        if ($exists) {
            throw new Exception("Error adding new category (already exists): $category_name $hashtag");
        }
        $exists = $dbw->selectRow(
            'yata_category',
            array('id'),
            array(
                'name'      => $category_name,
                'parent_id' => $parent_id
            )
        );

        if ($exists) {
            throw new Exception("Error adding new category (already exists): $category_name");
        }

        # make sure hashtags always start with a hash symbol: #
        if ($hashtag && substr($hashtag, 0,1) != '#') {
            $hashtag = '#'.$hashtag;
        }

        $dbw->insert(
            'yata_category',
            array(
                'name'        => $category_name,
                'hashtag'     => $hashtag,
                'description' => trim($description),
                'parent_id'   => $parent_id
            )
        );

    }

    public static function up_category($dbw, $ex_category_name, $args) {
        $category = self::get_category($dbw, $ex_category_name);
        if (!$category) {
            throw new Exception("Error updating category - no such category: $ex_category_name");
        }

        $set = array();
        if ($args["name"]) {
            # make sure this category does not exists yet
            $new_category_name = $args["name"];
            if ($category->parent_name) {
                $new_category_name = $category->parent_name . "/".$new_category;
            }
            $category_exists = self::get_category($dbw, $new_category_name);
            if ($category_exists && $category_exists->id != $category->id) {
                throw new Exception("Error updating category - already exists: $new_category_name");
            }
            $set["name"] = $args["name"];
        }
        if (array_key_exists("hashtag", $args) ) {
            if ($args["hashtag"] && substr($args["hashtag"], 0,1) != '#') {
                $args["hashtag"] = '#'.$args["hashtag"];
            }
            $set["hashtag"] = $args["hashtag"];
        }
        if (array_key_exists("description", $args) ) {
            $set["description"] = $args["description"];
        }
        if (array_key_exists("parent", $args) ) {
            # redefine the parent category
            if ($args["parent"]) {
                $parent_category = self::get_category($dbw, $args["parent"]);
                if (! $parent_category) {
                    throw new Exception("Error updating category - no such parent category: ". $args["parent"]);
                }
                else {
                    $set["parent_id"] = $parent_category->id;
                }
            }
            else {
                # define category as a top category (remove parent)
                $set["parent_id"] = null;
            }
        }
        if ($set) {
            $dbw->update(
                'yata_category',
                $set,
                array(
                    'id' => $category->id
                )
            );
        }
        else {
            throw new Exception("Error updating category - nothing needs to be updated.");
        }
        
    }

    public static function del_category($dbw, $category_name) {

        $cat_entry = self::get_category($dbw, $category_name);
        if (!$cat_entry) {
            throw new Exception("Error deleting category - no such category: $category_name");
        }

        $where = array();
        $where['ac.category_id'] = $cat_entry->id;

        $annotations_exist = $dbw->select(
            array(
                'a' => 'yata_annotation',
                'ac'=> 'yata_annotation_category',
                'p' => 'page',
            ),
            array(
                "title" => "p.page_title"
            ),
            $where,
            __METHOD__,
            array(
                'GROUP BY' => 'p.page_title'
            ),
            array(
                'ac' => array( 'LEFT JOIN', array( 'ac.annotation_id = a.id' ) ),
                'p'  => array( 'LEFT JOIN', array( 'p.page_id = a.page_id') ),
            )
        );

        $on_num_of_pages = $dbw->numRows($annotations_exist); 
        if ($on_num_of_pages > 0) {
            throw new Exception("Cannot delete category ".$category_name.", references exists on ".$on_num_of_pages . " pages");
        }
        
        $dbw->delete(
            'yata_category',
            array(
                id => $cat_entry->id
            )
        );
    }

    private static function split_cat_argstring($arg_string) {
        $args = array();
        # name='xxx', description='blabla', hashtag='#blabla', parent='parent/category'
        # splits argumentlist above and returns a dictionary instead.
        $allowed_attributes = array(
            "name","hashtag","description","parent"
        );
        $keyvals = preg_split('/\s*\,\s*(?=name|hashtag|description|parent)/', $arg_string);
        $re = '/\s*(?P<key>\w+)\s*\=\s*(?P<value>.*)/';
        foreach($keyvals as $keyval) {
            preg_match($re, $keyval,$matches);
            # remove ay leading and trailing  " or '
            # in the value
            $value = preg_replace('/^\s*(\'|\")/', '', $matches['value']);
            $value = preg_replace('/(\'|\")\s*$/', '', $value);
            $value = preg_replace('/\s*$/', '', $value);
            if (preg_match("/\=/", $value)) {
                throw new Exception("illegal character «=» in value: $value");
            }

            if (! in_array($matches['key'], $allowed_attributes)){
                throw new Exception("«".$matches['key'] . "» is not a valid attribute: name, hashtag, description, parent");
            }
            $args[$matches['key']] = $value;
        }
        return $args;
    }

    private static function category_add_del_callback($matches) {
        $dbw = wfGetDB( DB_MASTER );
        # split the arguments (after #annotcat:)
        #
        # {{#annotcat: add | name=new top category, hashtag=#shortcut, description=some meaningful comments, parent=grandparent_category/parent_category }}
        # {{#annotcat: up  | name=new top category, hashtag=#shortcut, description=some meaningful comments, parent=grandparent_category/parent_category }}
        # {{#annotcat: del | parent_category_name/category_name}}
        # {{#annotcat: del | #shortcut }}
        
        $splits = preg_split('/\s*\|\s*/', $matches["params"]);
        $method = isset($splits[0]) ? $splits[0] : null;
        $arg1   = isset($splits[1]) ? $splits[1] : null;
        $arg2   = isset($splits[2]) ? $splits[2] : null;
        
        if ( $method === "add" ) {
            $args = self::split_cat_argstring($arg1);
            self::add_category($dbw, $args['name'], $args['hashtag'], $args['description'], $args['parent']);
        }
        elseif ( $method === "up" ) {
            $args = self::split_cat_argstring($arg2);
            self::up_category($dbw, $arg1, $args);
        }
        elseif ( $method === "del" ) {
            # $arg_string already contains the category name, we do not have to split the arguments
            self::del_category($dbw, $arg1);
        }
        else {
            return $matches[0];
        }

        # remove the {{#annotcat: add|up|del }} commands from the source code
        # by returning an empty string
        return "";
    }

    public static function manage_categories( $dbw, $data) {
        # - handles {{#annotcat}} orders that add, update or delete categories
        # - returns the wikiText with the annotcat orders removed
         
        # look for all tags {{#annotcat:(.*)}}
        # and pass the matches to the callback function category_add_del_callback
        $data = preg_replace_callback(
            '/({{#annotcat:\s*(?P<params>.*?)}})/s',
            'self::category_add_del_callback',
            $data
        );
        return $data;
    }

    public static function get_annotation($dbr, $wikiPage, $bookmark) {
        $row = $dbr->selectRow(
            'yata_annotation',
            array('id', 'wiki_text', 'comment'),
            array(
                'page_id'  => $wikiPage->getId(),
                'bookmark' => $bookmark
            )
        );
        return $row;
    }

    public static function get_page_annotations($dbr, $page_id) {
        $order_by = array('ORDER BY' => 'name');
        $where = array(
            'pa.page_id' => $page_id
        );

        $annotations = $dbr->select(
            array('pa' => 'yata_page_annotation', 
                  'u'  => 'user'),
            array(
                'pa.id',
                'pa.page_id',
                'pa.name',
                'pa.value',
                'pa.insert_date',
                'u.user_name'
            ),
            $where,
            __METHOD__,
            $order_by,
            array(
                'u' => array( 'LEFT JOIN', array('u.user_id = pa.user_id') ) 
            )
        );
        return $annotations;
    }

    public static function update_page_annotation($dbw, $wikiPage, $key, $value) {
        $dbw->update(
            'yata_page_annotation',
            array(
                "value"         => $value,
                "modified_date" => $dbw->timestamp(),
            ),
            array(
                "page_id"  => $wikiPage->getId(),
                "name"     => $key
            )
        );
    }

    public static function insert_page_annotation($dbw, $wikiPage, $user, $key, $value) {
        $dbw->insert(
            'yata_page_annotation',
            array(
                "page_id"       => $wikiPage->getId(),
                "name"          => $key,
                "value"         => $value,
                "user_id"       => $user->getId(),
                "insert_date"   => $dbw->timestamp(),
                "modified_date" => $dbw->timestamp(),
            )
        );
    }

    public static function delete_page_annotation($dbw, $wikiPage, $key, $value) {
        $dbw->delete(
            'yata_page_annotation',
            array(
                "page_id" => $wikiPage->getId(),
                "name"    => $key,
                "value"   => $value,
            )
        );
    }

    public static function search_categories($dbr, $where, $order_by=NULL) {
        if (! $order_by) {
            $order_by = array('ORDER BY' => 'name');
        }

        $categories = $dbr->select(
            array('c' => 'yata_category'),
            array( 
                'c.id', 
                'c.name', 
                'c.hashtag', 
                'c.description', 
                'c.parent_id'
            ),
            $where,
            __METHOD__,
            $order_by
        );
        return $categories;
    }

    public static function get_categories($dbr, $category) {
        # return one ore many categories of a given name
        $category = trim($category);
        if (substr($category, 0,1) == '#') {
            $cat = self::get_category($dbr, $category);
            return array($cat);
        }
        if( strpos($category, '/')) {
            $cat = self::get_category($dbr, $category);
            return array($cat);
        }

        $where = array( 'name' => $category );
        $categories = self::search_categories($dbr, $where);
        $cats = array();
        foreach($categories as $category) {
            array_push($cats, $category);
        }
        return $cats;

        $selection = array( 
            'id'          => 'c.id', 
            'name'        => 'c.name', 
            'hashtag'     => 'c.hashtag', 
            'description' => 'c.description',
            'parent_name' => "''",
            'parent_id'   => 'c.parent_id' 
        );
        $rows = $dbr->select(
            array('c' =>'yata_category'),
            $selection,
            array(
                'name'    => $category,
            )
        );
        return $rows;
    }

    public static function get_category($dbr, $category) {
        $category = trim($category);

        $selection = array( 
            'id'          => 'c.id', 
            'name'        => 'c.name', 
            'hashtag'     => 'c.hashtag', 
            'description' => 'c.description',
            'parent_name' => "''",
            'parent_id'   => 'c.parent_id' 
        );

        # we received a hashtag: use that.
        if (substr($category, 0,1) == '#') {
            $row = $dbr->selectRow(
                array('c' =>'yata_category'),
                $selection,
                array(
                    'c.hashtag' => $category,
                )
            );
	        #print($dbr->lastQuery($row));
            return $row;
        }

        # get parent and child category: parent_cat/child_cat
        list($parent_cat, $child_cat) = preg_split('/\s*\/\s*/', $category);

        # we might only have a parent (top) category
        if (is_null($child_cat)) {
            # search for a top category (no parent)
            $row = $dbr->selectRow(
                array('c' =>'yata_category'),
                $selection,
                array(
                    'name'        => $parent_cat,
                    'parent_id'   => null
                )
            );
            return $row;
        }
        else {
            # search for category where parent matches
            # by self-joining table via parent_id
            $selection['parent_name'] = 'pc.name';
            $row = $dbr->selectRow(
                array('pc'=>'yata_category', 'c'=>'yata_category'),
                $selection,
                array(
                    'c.name' => $child_cat,
                    'pc.name' => $parent_cat,
                ),
                __METHOD__,
                array(),
                array( 'pc' => array( 'INNER JOIN', array ( 'c.parent_id = pc.id' ) ) )
            );
            return $row;
        } 
    }

    public static function get_category_and_parent_for_id($dbr, $id) {
        $row = $dbr->selectRow(
            array('c'=>'yata_category', 'pc'=>'yata_category'),
            array(
                'id'          => 'c.id',
                'name'        => 'c.name',
                'hashtag'     => 'c.hashtag',
                'description' => 'c.description',
                'parent_name' => 'pc.name',
                'parent_id'   => 'pc.id'
            ),
            array(
                'c.id' => $id
            ),
            __METHOD__,
            array(),
            array('pc' => array('LEFT JOIN', array('c.parent_id = pc.id') ) )
        );
        return $row;
    }

    public static function get_child_categories($dbr, $category, $level=0,
    $depth=null) {
        $rows = $dbr->select(
            array('yata_category'),
            array('id', 'name', 'hashtag', 'description', 'parent_id', "$level as level"),
            array(
                "parent_id" => $category->id
            )
        );

        $all_child_categories = array();

        foreach($rows as $row) {
            array_push($all_child_categories, $row);

            # if no $depth is defined (null),
            # recursively search for child categories
            if (is_null($depth)) {
                $child_categories = self::get_child_categories(
                    $dbr, $row, $level+1
                );

                foreach($child_categories as $child_category) {
                    array_push($all_child_categories, $child_category);
                }
            }
            # if $depth > 0, continue searching for
            # child categories
            elseif ($depth) {
                $child_categories = self::get_child_categories(
                    $dbr, $row, $level+1, $depth-1
                );

                foreach($child_categories as $child_category) {
                    array_push($all_child_categories, $child_category);
                }
            }
        }
        return $all_child_categories;
    }

    public static function delete_annotations($dbw, $wikiPage) {
        $dbw->delete(
            'yata_annotation',
            array(
                'page_id' => $wikiPage->getId()
            )
        );
    }

    public static function check_annotation_bookmark_exist($dbr, $wikiPage, $bookmark) {
        $row = $dbr->selectRow(
            array('yata_annotation'),
            array('id'),
            array(
                'bookmark' => $bookmark,
                'page_id'  => $wikiPage->getId()
            )
        );
        return $row;
    }

    public static function delete_annotation_categories($dbw, $wikiPage) {
        $dbw->delete(
            'yata_annotation_category',
            array(
                'page_id' => $wikiPage->getId()
            )
        );
    }

    public static function insert_annotation_category($dbw, $wikiPage, $annotation_id, $category_id) {
        $ex_ac = $dbw->selectRow(
            'yata_annotation_category',
            array('anz' => 'COUNT(*)'),
            array(
                'page_id'       => $wikiPage->getId(),
                'annotation_id' => $annotation_id,
                'category_id'   => $category_id
            )
        );
        if ($ex_ac->anz) {
            return;
        }
        else {
            $dbw->insert(
                'yata_annotation_category',
                array(
                    'page_id'       => $wikiPage->getId(),
                    'annotation_id' => $annotation_id,
                    'category_id'   => $category_id
                )
            );
        }
    }

    # create a unique identifier / bookmark for all
    # annotations which don't have one.
    # It should be long enough to be 99.999% unique on a page
    public static function create_bookmark() {
        $id = substr(
                str_shuffle(
                    str_repeat(
                        '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ',
                        mt_rand(1,10)
                    )
                
                ),
        1,4);
        return $id;
    }

    public static function extract_query_keyvalues($string) {
        // Match key-value pairs and its comperator in a given search string.
        // e.g. key>value, key>=value, key<value, key<=value, key<>value, key=value
        // Will match key and values that are:
        // within single quotes '     \'.*?(?<!\\\\)\'
        // within double quotes "     \".*?(?<!\\\\)\"
        // with no quotes at all (may contain no spaces or any comperator char): [^\<\>\=\s]+
        // allowed comperators: = <> >= <= < >

        $key_comp_vals = array();
        $re =
            '/(?P<key>\'.*?(?<!\\\\)\'|\".*?(?<!\\\\)\"|[^\<\>\=\s]+)'
            .'\s*?(?P<comp>\=|\<\>|\>\=|\>|\<\=|\<)\s*?'
            .'(?P<val>\'.*?(?<!\\\\)\'|\".*?(?<!\\\\)\"|[^\s]+)/';
        preg_match_all($re, $string, $matches, PREG_SET_ORDER, 0);
        foreach($matches as $match) {
            # trim keys and values: remove any spaces and quotes
            $key = trim($match["key"], " '\"");
            $val = trim($match["val"], " '\"");
            array_push($key_comp_vals, [$key, $match["comp"], $val]);
        }
        return $key_comp_vals;
    }

    # Main function to parse all annotations,
    # insert a random id (where it doesn't exist)
    public static function parse_annotations($dbw, &$data, $wikiPage){
        /*
        1. get all existing annotations which exist on that page.
        2. find out which annotations have been updated (using the unique identifier)
        3. find out which annotations have been added -> assign a unique identifier
        4. find out which annotations have been removed from the wikitext and delete them
        5. assign every annotation to one or more categories
        6. assign every annotation to this page
        */

        # match either #annot: or #annotend:
        # fetch all options too
        $annotations_found = preg_match_all(
            '/(?P<annotation>{{\s*#annot((?P<end>end)|(?P<page>page))?\s*:\s*(?P<opts>.*?)\s*}})/s', 
            $data, 
            $reg_params, 
            PREG_OFFSET_CAPTURE
        );

        # we can have annotations with an id or without.
        # The id was either entered by hand (for creating overlapping annotations)
        # or it was automatically generated.
        $annotations_with_id = array();
        $annotations = array();
        $page_annotations = array();

        # as we crawl through the found annotations and give them new ids,
        # we also recreate the wikitext containing all the new ids.
        $new_data = "";  
        $starts = array();
        $start_loc = 0;
        $end_loc   = 0;
        
        foreach($reg_params["annotation"] as $index => $value) {
            # $value[1] contains the location where the current tag has been found
            $end_loc = $value[1];
            # rewrite the source code to include new ids in the code
            $new_data = $new_data . substr($data, $start_loc, $end_loc-$start_loc);
            $start_loc = $value[1] + strlen($value[0]);

            # we encountered an annotation ending
            if ($reg_params["page"][$index][0] === "page") {
                $opts = $reg_params["opts"][$index][0];

                // Match key-value pairs.
                // see https://regex101.com/r/wK0eD2/1 for detailed explanations
                // Will match key and values that are:
                // within single quotes '     \'.*?(?<!\\\\)\'
                // within double quotes "     \".*?(?<!\\\\)\"
                // with no quotes at all (and no spaces): [^\s]+
                $re =
                 '/(?P<key>\'.*?(?<!\\\\)\'|\".*?(?<!\\\\)\"|[^\s]+)'
                 .'\s*?=\s*?'
                 .'(?P<val>\'.*?(?<!\\\\)\'|\".*?(?<!\\\\)\"|[^\s]+)/';
                preg_match_all($re, $opts, $matches, PREG_SET_ORDER, 0);
                foreach($matches as $match) {
                    # trim keys and values: remove any spaces and quotes
                    $key = trim($match["key"], " '\"");
                    $val = trim($match["val"], " '\"");
                    $page_annotations[$key] = $val;
                }
                $new_data = $new_data . "{{#annotpage: $opts}}";
            }

            # we encountered an annotation ending
            elseif ($reg_params["end"][$index][0] === "end") {
                $bookmark = $reg_params["opts"][$index][0];
                # we encountered an id: try to find its beginning
                if ($bookmark) {
                    if ( $annotations_with_id[$bookmark] ) {
                        # we fetch the new id we created.
                        $new_bookmark = $annotations_with_id[$bookmark];
                        $annotations[ $new_bookmark ]["end"] = 
                            $reg_params["end"][$index];

                        # extract the wiki text between the annotations
                        #$start_char = $annotations[$new_bookmark]["start"][1] 
                        #            + strlen($annotations[ $new_bookmark ]["start"][0]);
                        $start_char = $annotations[$new_bookmark]["start_char"];
                        $end_char   = $reg_params["annotation"][$index][1];
                        $annotations[$new_bookmark]["wiki_text"]  = 
                            substr($data, $start_char, $end_char-$start_char);

                        $new_data = $new_data . "{{#annotend:$new_bookmark}}";
                    }
                    else {
                        throw new Exception("found #annotend with bookmark=$bookmark but no annotation starts with this bookmark");
                    }
                }
                # we found an ending, but have not enough starts: there seems to be a start-ending mismatch
                elseif ( count($starts) == 0 ) {
                    throw new Exception("starts and ends do not match: I encountered an ending but no corresponding start.");
                }
                # we pop the last start from our stack to match it with its ending
                else {
                    $start = array_pop($starts);
                    $start_char = $start[1] + strlen($start[0]);
                    $end_char   = $reg_params["annotation"][$index][1];
        
                    $new_bookmark = $start["new_bookmark"];
                    $annotations[$new_bookmark] = array(
                        "is_new"     => true,
                        "categories" => $start["categories"],
                        "comment"    => $start["comment"],
                        "wiki_text"  => substr($data, $start_char, $end_char-$start_char)
                    );
                    $reg_params["annotation"][$index][2] = $new_bookmark;
                    $reg_params["annotation"][$index]["new_annot"] = "{{#annotend:$new_bookmark}}";
                    $new_data = $new_data . "{{#annotend:$new_bookmark}}";
                }
            }

            # we encountered an annotation START
            elseif ($reg_params["end"][$index][0] === "" && $reg_params["page"][$index][0] === "") {
                # split comment|$category|$bookmark by vertical bar
                $string = $reg_params["opts"][$index][0];
                $words = preg_split( '/\s*\|\s*/', $string); 
                $comment = isset($words[0])? $words[0] : null;
                $category = isset($words[1])? $words[1] : null;
                $bookmark = isset($words[2])? $words[2] : null;
                # more than one category can be split using a comma: 
                #   category1, category2
                # categories are by default hierarchically organized and must appear
                # in following formats:
                #   parent_category/child_category
                #   /top_category
                #   top_category
				$cat_strs = preg_split("/\s*\,\s*/", $category, -1, PREG_SPLIT_NO_EMPTY);
                $cats = array();
                foreach($cat_strs as $cat_str) {
                    # we got a category id, which consists only of numbers
                    if ( preg_match("/^\s*\d*\s*$/", $cat_str) ) {
                        $cat = self::get_category_and_parent_for_id($dbw, $cat_str);
                    }
                    else {
                        $cat = self::get_category($dbw, $cat_str);
                    }
                    if ($cat) {
                        array_push($cats, $cat);
                    }
                    else {
                        throw new Exception("no such category: $cat_str");
                    }
                }
                # we encountered an id: replace it with a unique one,
                # if it doesn't exist yet.
                $new_bookmark = 0;
                if ($bookmark) {
                    if ( self::check_annotation_bookmark_exist($dbw, $wikiPage, $bookmark))
                        {
                        # it is an existing annotation, we keep the id
                        $new_bookmark = $bookmark;
                    } 
                    else {
                        # we replace the manually entered id by a random one
                        $new_bookmark = self::create_bookmark();
                    }
                    $annotations_with_id[$bookmark] = $new_bookmark;
                    $annotations[$new_bookmark] = array();
                    $annotations[$new_bookmark]["start_char"] = $value[1] + strlen($value[0]);
                    $annotations[$new_bookmark]["is_new"] = true;
                    $annotations[$new_bookmark]["categories"] = $cats;
                    $annotations[$new_bookmark]["comment"] = $comment;
                }
                # we found no id, push it on the stack to find its 
                # ending counterpart later
                else {
                    $new_bookmark = self::create_bookmark();
                    $value["new_bookmark"] = $new_bookmark;
                    $value["categories"] = $cats;
                    $value["comment"] = $comment;
                    array_push($starts, $value);
                }

                # replace category names with their corresponding id's
                $new_category = join(',',
                    array_map(
                        function($row) { return $row->id; },
                        $cats
                    )
                );
                $new_data = $new_data . "{{#annot:$comment|$new_category|$new_bookmark}}";
            }

        }
        # add the end of the text
        $new_data = $new_data . substr($data, $start_loc, strlen($data)-$start_loc);

        # replace the original wikiText
        $data = $new_data;
        return array($annotations, $page_annotations);
    }


    //
    // Schema updates, called on Vagrant machine with: mwscript update.php
    // otherwise: cd maintenance; php update.php
    // creates the necessary tables for YATA.
    public static function onLoadExtensionSchemaUpdates( DatabaseUpdater $updater = null ) {

        $map = [ 'mysql', 'postgres' ];
        $type = $updater->getDB()->getType();

        if ( !in_array( $type, $map ) ) {
            throw new Exception( "YATA extension does not currently support $type database." );
        }
        $sql = __DIR__ . '/sql/yata.' . $type . '.sql';
        $updater->addExtensionTable( 'yata', $sql );

        return true;
 }

    // called right after «Save Changes»
    // when an error is present, returns to edit page and prints the error
    // may be used to check whether the syntax of the annotations is correct
    public static function onEditFilter($editor, $text, $section, &$error, $summary) {
    }

    // this is called after �Save changes�
    // and after onEditFilter
    public static function onArticlePrepareTextForEdit($wikiPage, $popts) {
    }

    // first hook called when editing a page
    // even before any checks whether a user is allowed to
    public static function onAlternateEdit($editPage) {
    }

    // This hook is called after hitting the «edit» button of a page.
    // It transforms all categories from their internal ID into readable text.
    // This allows us to change the category names later without having
    // to change all source code where this category occurs.
    public static function onEditFormInitialText($editPage) {
        $data = $editPage->textbox1;
        $data =  preg_replace_callback(
            '/(?:{{#annot:(?P<params>.*?)}})/s',
            'self::replace_id_with_category',
            $data
        );
        // assign the new content
        $editPage->textbox1 = $data;
    }

    // preload edit forms with some content
    public static function onEditFormPreloadText(&$text, &$title) {
    }

    // callback function for onEditFormInitialText hook
    // all category_id's are replaced with the category names themselves
    // using the hashtag (if available) or the parent_category/child_category naming scheme (fallback).
    private static function replace_id_with_category($matches) {
        $dbw = wfGetDB( DB_MASTER );
        
        $splits = preg_split('/\s*\|\s*/', $matches["params"]);
        $comment = isset($splits[0]) ? $splits[0] : null;
        $cat     = isset($splits[1]) ? $splits[1] : null;
        $bookmark= isset($splits[2]) ? $splits[2] : null;
        
        if (!$cat) {
            return $matches[0];
        }
        # TODO: check whether category is numeric
        $categories = preg_split('/\s*,\s*/', $cat);
        $cat_and_parents = array();
        foreach($categories as $category) {
            $cat_and_parent = self::get_category_and_parent_for_id($dbw, $category);
            if ($cat_and_parent) {
                if ($cat_and_parent->hashtag) {
		    array_push($cat_and_parents, $cat_and_parent->hashtag);
                }
                elseif ($cat_and_parent->parent_name) {
                    array_push($cat_and_parents, $cat_and_parent->parent_name .'/'. $cat_and_parent->name);
                }
                else {
                    array_push($cat_and_parents, $cat_and_parent->name);
                }
            }
        }
        $annot = "{{#annot: " . $comment." | ";
        $annot.= join(', ', $cat_and_parents);
        $annot.= " | $bookmark}}";

        return $annot;
    }

    private static function render_math_content($matches){
        
        global $wgDefaultUserOptions;
        $mathMode = $wgDefaultUserOptions['math'];

        $unescapedExtract = $matches["math_element"];
        $unescapedExtract = Parser::extractTagsAndParams( array( 'math' ), $unescapedExtract, $mathTags );
        foreach ( $mathTags as $id => $tag ) {
            $content     = $tag[1];
            $attributes  = $tag[2];
            $fullElement = $tag[3];
            $renderer = MathRenderer::getRenderer( $content, $attributes, $mathMode );

            if ( preg_match( '#</\s*math\s*>$#', $fullElement) ) { // only full elements
                $renderer = MathRenderer::getRenderer( $content, $attributes, $mathMode );

                if ( $renderer->checkTex() && $renderer->render() ) {
                    $renderedMath = $renderer->getHtmlOutput();
                    $renderer->writeCache(); // Math rendering takes a while
                    return $renderedMath;

                    #$logger->info( "Display \\$$content\\$ in search results." );
                } 
                else {
                    var_dump($renderer->getLastError());
                    #die("did not render!");
                    #$logger->warning( "Can not display \\$$content\\$ in search results. \n".  var_export( $renderer->getLastError(), true ) );
                }
            }
            // undo parser modifications to unclosed math tags or in the case of a rendering error
            $unescapedExtract = str_replace( $id, $fullElement, $unescapedExtract );
        }
        return $unescapedExtract;
    }


    public static function onParserBeforeTidy( Parser &$parser, string &$text ) { 
        $text = preg_replace_callback(
            '/ENCODED_YATA_CONTENT (?P<img64>[[0-9a-zA-Z\/+=]*) END_ENCODED_YATA_CONTENT/sm',
            'self::decode_math_images',
            $text
        );
        return true;
    }

    public static function decode_math_images($matches) {
        return base64_decode($matches["img64"]);
    }
}

