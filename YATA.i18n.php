<?php
/**
 * @since 1.0.0
 *
 * @file
 *
 * @licence GNU GPL
 * @author Swen Vermeul (vermeul)
 */

$magicWords = array();

/** English
 * @author Swen Vermeul (vermeul)
 */
$magicWords['en'] = array(
   'annot'    => array( 0, 'annot' ),
   'annotpage' => array( 0, 'annotpage' ),
   'annotend' => array( 0, 'annotend' ),
   'annotlist' => array( 0, 'annotlist' ),
   'annotask' => array( 0, 'annotask' ),
   'annotcat' => array( 0, 'annotcat'),
);
